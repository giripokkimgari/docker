# ==== Stage 1: Application Build ====
FROM maven:3.8.1-openjdk-11 AS build

WORKDIR /app 

# Copy only POM file to cache dependencies
# The dependencies are copied separately from the source code, allowing Docker to cache dependencies when the POM file hasn't changed. 
# This helps speed up the build process by utilizing layer caching.
COPY pom.xml .

# Download th eporject dependencies and store them in Docker image.
# This is done separately from copying the source code to optimize layer caching.
RUN mvn dependency:go-offline 

# Copy the rest of source code
COPY src src 

# Build the application
RUN mvn package -DskipTests

# Download dependency jars for caching using multiple RUN 
RUN wget -O gson.jar https://repo.maven.apache.org/maven2/com/google/code/gson/gson/2.8.9/gson-2.8.9.jar
RUN wget -O postgres.jar https://repo.maven.apache.org/maven2/org/postgresql/postgresql/42.2.20/postgresql-42.2.20.jar


# === Stage 2: Package ====
# Start with a base image that already has Java installed
FROM openjdk:11-jre-slim

ARG BUILD_DATE
ARG VCS_REF

# Add metadata to the image to describe how to run the application
LABEL version="1.0" \
      description="Spring Boot Application Docker Image" \
      maintainer="Giri<giri@gamil.com>" \
      #Include metadata about the build in the image
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-ref=$VCS_REF

# Set Env variables
ENV APP_NAME=demo-app \
    SPRING_PROFILE=dev

# Set up user with limited permissions
RUN \
  groupadd -r appgroup && \
  useradd -r -g appgroup appuser && \
  mkdir /opt/app && \ 
  chown -R appuser:appgroup /opt/app && \
  # Remove unnecessary dependencies and temporary files to reduce the size of the final image
  apt-get purge -y --auto-remove && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set the working directory for any ADD, COPY, CMD, ENTRYPOINT, or RUN instructions that follow.
WORKDIR /opt/app

# Copy only the necessary artifacts from the previous build stage
COPY --from=build --chown=appuser:appgroup /app/target/demo-app.jar /opt/app/app.jar
COPY --from=build --chown=appuser:appgroup /app/*.jar /opt/app/

# Expose ports
EXPOSE 8080

# This instruction adds a health check to your container.
# It uses curl to make a request to the /actuator/health endpoint and exits with a non-zero status if the check fails.
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 \
    CMD curl -f http://localhost:8080/actuator/health || exit 1


#Set the user name to use for running any RUN, CMD, and ENTRYPOINT instructions that follow.
USER appuser

# Set default command to run when the conatiner starts
CMD ["java","-jar","/opt/app/app.jar"]

# default command to set while starting the container
#ENTRYPOINT ["java","-jar","app.jar"]
